package com.example.assignment_2_java_data_access_and_display;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment2JavaDataAccessAndDisplayApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment2JavaDataAccessAndDisplayApplication.class, args);
    }

}
