package com.example.assignment_2_java_data_access_and_display.data_access;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConnectionHelperTest {

    private ConnectionHelper connectionHelper;

    @BeforeEach
    void setUp() {
         connectionHelper = new ConnectionHelper();
    }
    @Test
    void getConnection_shouldNotBeNull() {
        connectionHelper.openConnection();
        assertNotNull(connectionHelper.getConnection());
        connectionHelper.closeConnection();
    }
    @Test
    void openConnection_shouldReturnTrue() {
        assertTrue(connectionHelper.openConnection());
        connectionHelper.closeConnection();
    }

    @Test
    void closeConnection_shouldReturnTrue() {
        connectionHelper.openConnection();
        assertTrue(connectionHelper.closeConnection());
    }
}