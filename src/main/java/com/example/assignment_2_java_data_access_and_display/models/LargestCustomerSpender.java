package com.example.assignment_2_java_data_access_and_display.models;

public class LargestCustomerSpender {

    private final int customerId;
    private final double totalCostInvoices;

    /**
     * Constructor for creating a new LargestCustomerSpender.
     *
     * @param customerId        The ID of the Customer.
     * @param totalCostInvoices The total cost of this customers invoices.
     */
    public LargestCustomerSpender(int customerId, double totalCostInvoices) {
        this.customerId = customerId;
        this.totalCostInvoices = totalCostInvoices;
    }

    /**
     * Method for getting the Customer ID.
     *
     * @return Returns the ID of the Customer as an int.
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Method for getting total cost of the customers invoices.
     *
     * @return Returns the total cost of the customers invoice as a double.
     */
    public double getTotalCostInvoices() {
        return totalCostInvoices;
    }
}
