package com.example.assignment_2_java_data_access_and_display.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This class serves the purpose of holding the connection URL
 * This means there is only one place in the application where the connection string needs to be written.
 */
public class ConnectionHelper {

    private static final String CONNECTION_URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection connection = null;

    public Connection getConnection() {
        return connection;
    }

    /**
     * Used to connect to a DB, specified by the CONNECTION_URL.
     *
     * @return true if connection was successful, else false.
     */
    public boolean openConnection() {
        try {
            // Open connection
            connection = DriverManager.getConnection(CONNECTION_URL);
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * Used to disconnect from the DB.
     *
     * @return True if successfully disconnected, else false.
     */
    public boolean closeConnection() {
        try {
            // Closing connection
            connection.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Failed to close SQLite connection.");
            System.out.println(e.getMessage());
            return false;
        }
    }
}

