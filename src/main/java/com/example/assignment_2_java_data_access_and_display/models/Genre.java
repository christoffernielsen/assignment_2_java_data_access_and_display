package com.example.assignment_2_java_data_access_and_display.models;

public class Genre {

    private final int id;
    private final String name;

    /**
     * Constructor for creating a new Genre.
     *
     * @param id   The ID of the Genre (primary key in database).
     * @param name The name of the Genre. Eg. Rock, Latin, Jazz etc.
     */
    public Genre(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Method for getting the ID of the Genre.
     *
     * @return Returns the ID of the Genre as an int.
     */
    public int getId() {
        return id;
    }

    /**
     * Method for getting name of the Genre.
     *
     * @return Returns the name of the Genre as a String.
     */
    public String getName() {
        return name;
    }
}
