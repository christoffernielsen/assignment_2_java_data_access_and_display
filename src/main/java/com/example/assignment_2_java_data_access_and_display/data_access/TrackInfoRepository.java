package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.TrackInfo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class TrackInfoRepository {

    private final ConnectionHelper connectionHelper = new ConnectionHelper();

    public ArrayList<TrackInfo> getTrackInfoBySearchingTrackName(String searchString) {
        connectionHelper.openConnection();
        ArrayList<TrackInfo> trackInfos = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT tr.TrackId, tr.Name AS Track, ar.Name AS Artist ,al.Title AS Album, ge.Name AS Genre " +
                            "FROM Track tr " +
                            "INNER JOIN Album al ON tr.AlbumId = al.AlbumId " +
                            "INNER JOIN Artist ar ON al.ArtistId = ar.ArtistId " +
                            "INNER JOIN Genre ge ON tr.GenreId = ge.GenreId " +
                            "WHERE tr.Name LIKE ?");
            preparedStatement.setString(1, "%" + searchString + "%");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                trackInfos.add(new TrackInfo(
                        resultSet.getInt("TrackId"),
                        resultSet.getString("Track"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre")
                ));
            }


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return trackInfos;
    }


}
