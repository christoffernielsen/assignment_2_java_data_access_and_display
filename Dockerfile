FROM openjdk:16
ADD build/libs/assignment_2_java_data_access_and_display-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]