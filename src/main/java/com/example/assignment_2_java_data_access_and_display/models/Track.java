package com.example.assignment_2_java_data_access_and_display.models;

public class Track {

    private final int id;
    private final int albumId;
    private final int genreId;
    private final String name;

    /**
     * Constructor for creating a new Track.
     *
     * @param id      The ID of the track (primary key in database).
     * @param albumId The ID of the Album where track can be found. (foreign key in database).
     * @param genreId The ID of the Genre that the track has. (foreign key in database).
     * @param name    The name of the Track.
     */
    public Track(int id, int albumId, int genreId, String name) {
        this.id = id;
        this.albumId = albumId;
        this.genreId = genreId;
        this.name = name;
    }

    /**
     * Method for getting the ID of the Track.
     *
     * @return Returns ID of the Track as an int.
     */
    public int getId() {
        return id;
    }

    /**
     * Method for getting Album ID of the Track.
     *
     * @return Returns Album ID of the Track as an int.
     */
    public int getAlbumId() {
        return albumId;
    }

    /**
     * Method for getting Genre ID of the Track.
     *
     * @return Returns Genre ID of the Track as an int.
     */
    public int getGenreId() {
        return genreId;
    }

    /**
     * Method for getting name of the Track.
     *
     * @return Returns name of the Track as a String.
     */
    public String getName() {
        return name;
    }
}
