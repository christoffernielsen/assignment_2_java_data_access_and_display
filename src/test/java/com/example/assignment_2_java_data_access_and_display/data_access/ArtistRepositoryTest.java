package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.Artist;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArtistRepositoryTest {

    private ArtistRepository artistRepository;

    @BeforeEach
    void setUp() {
        artistRepository = new ArtistRepository();
    }

    @Test
    void getArtistById_artistId2_shouldNotThrowSqlException() {
        assertDoesNotThrow(() -> artistRepository.getArtistById(2));
    }

    @Test
    void getArtistById_artistId3_shouldBeValid() {
        Artist artist = artistRepository.getArtistById(3);
        assertEquals(3, artist.getId());
        assertEquals("Aerosmith", artist.getArtistName());
    }

    @Test
    void getMaxArtistId_shouldReturn275() {
        assertEquals(275, artistRepository.getMaxArtistId());
    }

    @Test
    void get5RandomArtists_shouldReturnArrayListOfArtistTheSizeOf5() {
        ArrayList<Artist> artists = artistRepository.get5RandomArtists();
        assertEquals(5, artists.size());
    }
}
