package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.Artist;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

public class ArtistRepository {
    private final ConnectionHelper connectionHelper = new ConnectionHelper();

    /**
     * Fetches the artist corresponding to the artistId parameter.
     *
     * @param artistId Id of the artist we want.
     * @return The artist object, if found.
     * @throws SQLException
     */
    public Artist getArtistById(int artistId) {
        connectionHelper.openConnection();
        Artist artist = null;
        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT ArtistId, Name " +
                            "FROM Artist " +
                            "WHERE ArtistId = ?");
            preparedStatement.setInt(1, artistId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                artist = new Artist(
                        resultSet.getInt("ArtistId"),
                        resultSet.getString("Name")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }


        return artist;
    }

    /**
     * Finds the max artistId in the DB, used when fetching random artists.
     *
     * @return max artistId in DB.
     * @throws SQLException
     */
    public int getMaxArtistId() {
        connectionHelper.openConnection();
        int maxArtistId = 0;

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT MAX(ArtistId) AS ArtistId " +
                            "FROM Artist");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                maxArtistId = resultSet.getInt("ArtistId");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return maxArtistId;
    }

    /**
     * Fetches 5 random artists from the DB.
     *
     * @return An arrayList containing 5, unique, random artists.
     * @throws SQLException
     */
    public ArrayList<Artist> get5RandomArtists() {
        ArrayList<Artist> artists = new ArrayList<>();
        Random random = new Random();
        int maxArtistId = getMaxArtistId();
        ArrayList<Integer> previousRandomNumbers = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            // Check if this random statement is correct
            int randomNumber = random.nextInt(maxArtistId) + 1;

            while (previousRandomNumbers.contains(randomNumber)) {
                randomNumber = random.nextInt(maxArtistId) + 1;
            }
            artists.add(getArtistById(randomNumber));
            previousRandomNumbers.add(randomNumber);
        }
        return artists;
    }
}
