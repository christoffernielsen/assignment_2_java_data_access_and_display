package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.Track;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;


public class TrackRepository {

    private final ConnectionHelper connectionHelper = new ConnectionHelper();

    /**
     * Contains Sql-query to get a specific track in the DB.
     *
     * @param trackId The id of the track we want.
     * @return A track object.
     * @throws SQLException
     */
    public Track getTrackById(int trackId) {
        connectionHelper.openConnection();
        Track track = null;
        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT TrackId, AlbumId, GenreId, Name " +
                            "FROM Track " +
                            "WHERE TrackId = ?");
            preparedStatement.setInt(1, trackId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                track = new Track(
                        resultSet.getInt("TrackId"),
                        resultSet.getInt("AlbumId"),
                        resultSet.getInt("GenreId"),
                        resultSet.getString("Name")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return track;
    }

    /**
     * Contains Sql-query to find a specific track in the DB.
     *
     * @param trackName the name of the track we want.
     * @return An arrayList of tracks corresponding to all or some of the trackName.
     * @throws SQLException
     */
    public ArrayList<Track> getTrackByName(String trackName) {

        ArrayList<Track> tracks = new ArrayList<>();

        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT TrackId, AlbumId, GenreId, Name " +
                            "FROM Track " +
                            "WHERE Name LIKE ?");
            preparedStatement.setString(1, "%" + trackName + "%");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(new Track(
                        resultSet.getInt("TrackId"),
                        resultSet.getInt("AlbumId"),
                        resultSet.getInt("GenreId"),
                        resultSet.getString("Name")
                ));
            }


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return tracks;
    }

    /**
     * Contains Sql-query to find the highest track id in the DB.
     *
     * @return An int corresponding to the highest id.
     * @throws SQLException
     */
    public int getMaxTrackId() {
        connectionHelper.openConnection();
        int maxTrackId = 0;

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT MAX(TrackId) AS TrackId " +
                            "FROM Track");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                maxTrackId = resultSet.getInt("TrackId");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return maxTrackId;
    }

    /**
     * Contains Sql-query to fetch 5 random, unique, tracks from the DB.
     *
     * @return An arrayList with track objects.
     * @throws SQLException
     */
    public ArrayList<Track> get5RandomTracks() {

        ArrayList<Track> tracks = new ArrayList<>();
        Random random = new Random();
        int maxTrackId = getMaxTrackId();
        ArrayList<Integer> previousRandomNumbers = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            // Check if this random statement is correct
            int randomNumber = random.nextInt(maxTrackId) + 1;

            while (previousRandomNumbers.contains(randomNumber)) {
                randomNumber = random.nextInt(maxTrackId) + 1;
            }
            tracks.add(getTrackById(randomNumber));

            previousRandomNumbers.add(randomNumber);
        }

        return tracks;
    }
}
