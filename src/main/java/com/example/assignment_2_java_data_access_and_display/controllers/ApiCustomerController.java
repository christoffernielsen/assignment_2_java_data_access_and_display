package com.example.assignment_2_java_data_access_and_display.controllers;

import com.example.assignment_2_java_data_access_and_display.data_access.CustomerRepository;
import com.example.assignment_2_java_data_access_and_display.models.Customer;
import com.example.assignment_2_java_data_access_and_display.models.CustomersInCountry;
import com.example.assignment_2_java_data_access_and_display.models.MostPopularCustomerGenre;
import com.example.assignment_2_java_data_access_and_display.models.LargestCustomerSpender;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;

@RestController
public class ApiCustomerController {

    private CustomerRepository customerRepository = new CustomerRepository();

    /**
     * Fetches all customers from DB.
     *
     * @return An arraylist containing all customers in DB.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    /**
     * Fetches the customer with an id corresponding to the customerId parameter.
     *
     * @param customerId The id of the customer we're looking for.
     * @return a customer model.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customer/{customerId}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable int customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    /**
     * Fetches the customer with a name corresponding to the first and last name parameter.
     *
     * @param firstName The first name of the customer we're looking for.
     * @param lastName  The last name of the customer we're looking for.
     * @return a customer model.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customer")
    @ResponseBody
    public Customer getCustomerByName(@RequestParam(name = "firstName") String firstName,
                                      @RequestParam(name = "lastName") String lastName) {
        return customerRepository.getCustomerByName(firstName, lastName);
    }

    /**
     * Fetches a specified amount of customers from a certain point in the DB.
     *
     * @param limit  the amount of customers we want.
     * @param offset at which point in the list we start fetching customers.
     * @return An arraylist containing the customers.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customers")
    @ResponseBody
    public ArrayList<Customer> getCustomerByOffsetAndLimit(@RequestParam(name = "limit") int limit,
                                                           @RequestParam(name = "offset") int offset) {
        return customerRepository.getOffsetCustomers(limit, offset);
    }

    /**
     * Add a new customer to the DB.
     *
     * @param customer The customer we want to add.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customer", method = RequestMethod.POST)
    public void addNewCustomer(@RequestBody Customer customer){
        customerRepository.addNewCustomer(customer);
    }

    /**
     * Finds the customer with corresponding Id and updates it.
     *
     * @param customerId The id of the customer we want to update.
     * @param customer   A customer model containing updated variables.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customer/{customerId}", method = RequestMethod.PUT)
    public void updateExistingCustomer(@PathVariable int customerId,
                                       @RequestBody Customer customer) {
        customerRepository.updateCustomer(customerId, customer);
    }

    /**
     * Fetches the amount of customers in each country in the DB.
     *
     * @return an arraylist of customerCountry objects.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customers/perCountry", method = RequestMethod.GET)
    public ArrayList<CustomersInCountry> getCustomersPerCountry() {
        return customerRepository.getCustomersPerCountry();
    }

    /**
     * Fetches the customers in DB, sorted by who spent the most, descending.
     *
     * @return Arraylist of customerSpender objects.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customers/highestSpender", method = RequestMethod.GET)
    public ArrayList<LargestCustomerSpender> getHighestCustomerSpender() {
        return customerRepository.getHighestCustomerSpender();
    }

    /**
     * Fetches the Customers most favoured genre.
     *
     * @param customerId used to specify which customer to look for.
     * @return A CustomerGenre object.
     * @throws SQLException
     */
    @RequestMapping(value = "/api/customer/{customerId}/mostPopularGenre", method = RequestMethod.GET)
    public ArrayList<MostPopularCustomerGenre> getMostPopularCustomerGenre(@PathVariable int customerId) {
        return customerRepository.getMostPopularCustomerGenre(customerId);
    }


}
