The second assignment for Experis Academy

In this assignment we have developed a simple web api to push and fetch data to and from a chinook database.
We also built views using Thymeleaf and published the website using Heroku with Docker.

Link to Heroku application: **https://assignment2-pontus-christoffer.herokuapp.com/**

## GET Requests

Can be found in PostmanJSONFiles folder.  
The name of the json-file is: **GetRequests.postman_collection.json**

## POST Requests

Can be found in PostmanJSONFiles folder.  
The name of the json-file is: **PostRequests.postman_collection.json**

## PUT Requests

Can be found in PostmanJSONFiles folder.  
The name of the json-file is: **PutRequests.postman_collection.json**
