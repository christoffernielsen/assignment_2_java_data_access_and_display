package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.Genre;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

public class GenreRepository {
    private final ConnectionHelper connectionHelper = new ConnectionHelper();

    /**
     * Contains Sql-query to find a specific genre.
     *
     * @param genreId The id of the genre we're looking for.
     * @return a genre object.
     * @throws SQLException
     */
    public Genre getGenreById(int genreId) {
        connectionHelper.openConnection();
        Genre genre = null;
        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT GenreId, Name " +
                            "FROM Genre " +
                            "WHERE GenreID = ?");
            preparedStatement.setInt(1, genreId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genre = new Genre(
                        resultSet.getInt("GenreId"),
                        resultSet.getString("Name")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return genre;
    }

    /**
     * Contains Sql-query to find the highest genreId in the DB, used to fetch random genres.
     *
     * @return The highest genreId in the DB.
     * @throws SQLException
     */
    public int getMaxGenreId(){
        connectionHelper.openConnection();
        int maxGenreId = 0;

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT MAX(GenreId) AS GenreId " +
                            "FROM Genre");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                maxGenreId = resultSet.getInt("GenreId");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return maxGenreId;
    }

    /**
     * Fetches 5 random, unique, genres from the DB.
     *
     * @return An arrayList of genre objects.
     * @throws SQLException
     */
    public ArrayList<Genre> get5RandomGenres() {

        ArrayList<Genre> genres = new ArrayList<>();
        Random random = new Random();
        int maxGenreId = getMaxGenreId();
        ArrayList<Integer> previousRandomNumbers = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            // Check if this random statement is correct
            int randomNumber = random.nextInt(maxGenreId) + 1;

            while (previousRandomNumbers.contains(randomNumber)) {
                randomNumber = random.nextInt(maxGenreId) + 1;
            }

            genres.add(getGenreById(randomNumber));

            previousRandomNumbers.add(randomNumber);
        }

        return genres;
    }
}
