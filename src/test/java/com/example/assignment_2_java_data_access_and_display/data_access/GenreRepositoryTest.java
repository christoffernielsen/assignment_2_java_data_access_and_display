package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GenreRepositoryTest {

    private GenreRepository genreRepository;

    @BeforeEach
    void setUp(){
        genreRepository = new GenreRepository();
    }
    @Test
    void getGenreById_genreId2_shouldNotThrowSqlException(){
        assertDoesNotThrow(() -> genreRepository.getGenreById(2));
    }

    @Test
    void getGenreById_genreId3_shouldGetCorrectGenreInformation() {
        Genre genre = genreRepository.getGenreById(3);
        assertEquals(3, genre.getId());
        assertEquals("Metal", genre.getName());
    }

    @Test
    void getMaxGenreId_shouldReturn25() {
        assertEquals(25, genreRepository.getMaxGenreId());
    }

    @Test
    void get5RandomGenres_shouldReturnArrayListOfGenresTheSizeOf5() {
        ArrayList<Genre> genres = genreRepository.get5RandomGenres();
        assertEquals(5, genres.size());
    }
}
