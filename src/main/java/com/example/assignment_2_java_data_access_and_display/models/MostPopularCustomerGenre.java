package com.example.assignment_2_java_data_access_and_display.models;

public class MostPopularCustomerGenre {

    private final String genre;
    private final int count;

    /**
     * Constructor for creating a new MostPopularCustomerGenre.
     *
     * @param genre The name of the genre.
     * @param count The count of a customers most popular genre.
     */
    public MostPopularCustomerGenre(String genre, int count) {
        this.genre = genre;
        this.count = count;
    }

    /**
     * Method for getting the most popular genre of a customer.
     *
     * @return Returns the genre as a String.
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Method for getting the count of a customers most popular genre.
     *
     * @return Returns the count of a customers most popular genre as an int.
     */
    public int getCount() {
        return count;
    }
}
