package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.Track;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TrackRepositoryTest {

    private TrackRepository trackRepository;

    @BeforeEach
    void setUp(){
        trackRepository = new TrackRepository();
    }
    @Test
    void getTrackById_trackId2_shouldNotTrowSqlException(){
        assertDoesNotThrow(() -> trackRepository.getTrackById(2));
    }

    @Test
    void getTrackById_trackId10_shouldReturnCorrectTrackInformation() {
        Track track = trackRepository.getTrackById(12);
        assertEquals(12, track.getId());
        assertEquals("Breaking The Rules", track.getName());
        assertEquals(1, track.getAlbumId());
        assertEquals(1, track.getGenreId());
    }

    @Test
    void getTrackByName_trackNameGoDown_shouldNotTrowSqlException(){
        assertDoesNotThrow(() -> trackRepository.getTrackByName("Go Down"));
    }

    @Test
    void getTrackByName_trackName_shouldReturnCorrectTrackInformation() {
        ArrayList<Track> tracks = trackRepository.getTrackByName("Overdose");
        Track track = tracks.get(0);
        assertEquals(20, track.getId());
        assertEquals("Overdose", track.getName());
        assertEquals(4, track.getAlbumId());
        assertEquals(1, track.getGenreId());
    }

    @Test
    void getMaxTrackId_shouldReturn3503() {
        assertEquals(3503, trackRepository.getMaxTrackId());
    }

    @Test
    void get5RandomTracks_shouldReturnArrayListOfTracksTheSizeOf5() {
        ArrayList<Track> tracks = trackRepository.get5RandomTracks();
        assertEquals(5, tracks.size());
    }
}
