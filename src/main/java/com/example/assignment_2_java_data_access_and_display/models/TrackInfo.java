package com.example.assignment_2_java_data_access_and_display.models;

public class TrackInfo {

    private final int trackId;
    private final String trackName;
    private final String artistName;
    private final String albumTitle;
    private final String genre;

    /**
     * Constructor for creating a new TrackInfo.
     * Keeps track of relevant information for a Track such as Artist, Album and Genre.
     *
     * @param trackId    The ID of the specific Track.
     * @param trackName  The name of the Track.
     * @param artistName The name of the Artist that made the track.
     * @param albumTitle The title of the Album that made the track.
     * @param genre      The genre of the track.
     */
    public TrackInfo(int trackId, String trackName, String artistName, String albumTitle, String genre) {
        this.trackId = trackId;
        this.trackName = trackName;
        this.artistName = artistName;
        this.albumTitle = albumTitle;
        this.genre = genre;
    }

    /**
     * Method for getting the Track ID.
     *
     * @return Returns the Track ID as an int.
     */
    public int getTrackId() {
        return trackId;
    }

    /**
     * Method for getting name of the Track.
     *
     * @return Returns the name of the Track as a String.
     */
    public String getTrackName() {
        return trackName;
    }

    /**
     * Method for getting name of Artist that made the Track.
     *
     * @return Returns the name of the Artist as a String.
     */
    public String getArtistName() {
        return artistName;
    }

    /**
     * Method for getting title of the Album where the Track can be found.
     *
     * @return Returns the title of the Album as a String.
     */
    public String getAlbumTitle() {
        return albumTitle;
    }

    /**
     * Method for getting the Genre of the Track.
     *
     * @return Returns the Genre of the Track as a String.
     */
    public String getGenre() {
        return genre;
    }
}
