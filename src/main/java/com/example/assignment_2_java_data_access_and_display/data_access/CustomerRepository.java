package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.Customer;
import com.example.assignment_2_java_data_access_and_display.models.CustomersInCountry;
import com.example.assignment_2_java_data_access_and_display.models.LargestCustomerSpender;
import com.example.assignment_2_java_data_access_and_display.models.MostPopularCustomerGenre;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CustomerRepository {

    private final ConnectionHelper connectionHelper = new ConnectionHelper();

    /**
     * Contains Sql-query to get all the customers from DB.
     *
     * @return An arraylist containing all the customers.
     * @throws SQLException
     */
    public ArrayList<Customer> getAllCustomers() {

        ArrayList<Customer> customers = new ArrayList<>();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                            "FROM Customer");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return customers;
    }

    /**
     * Contains Sql-query to get the customer with
     * a customerId corresponding to the customerId parameter
     *
     * @param customerId Id of the customer we want.
     * @return A customer object containing the data of the found customer.
     * @throws SQLException
     */
    public Customer getCustomerById(int customerId) {
        connectionHelper.openConnection();
        Customer customer = null;
        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                            "FROM Customer " +
                            "WHERE CustomerId = ?");
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return customer;
    }

    /**
     * Contains Sql-query to get the customer with
     * a first and lastname corresponding to the first and lastname parameters.
     *
     * @param firstName The firstname of the customer we want.
     * @param lastName  the lastname of the customer we want.
     * @return A customer object containing the data of the found customer.
     * @throws SQLException
     */
    public Customer getCustomerByName(String firstName, String lastName){
        Customer customer = null;
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                            "FROM Customer " +
                            "WHERE FirstName LIKE ? AND LastName LIKE ?");
            preparedStatement.setString(1, "%" + firstName + "%");
            preparedStatement.setString(2, "%" + lastName + "%");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return customer;
    }

    /**
     * Contains Sql-query to fetch a specified amount of customers, offset from the start of the DB.
     *
     * @param limit  how many customers we want to fetch.
     * @param offset were to start collecting customers.
     * @return An arraylist containing the customers.
     * @throws SQLException
     */
    public ArrayList<Customer> getOffsetCustomers(int limit, int offset) {

        ArrayList<Customer> customers = new ArrayList<>();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                            "FROM Customer LIMIT ? OFFSET ?");
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return customers;
    }

    /**
     * Contains Sql-query to POST a new customer to the DB.
     *
     * @param customer The customer we want to add to the DB.
     * @return True if successful, else false.
     * @throws SQLException
     */
    public boolean addNewCustomer(Customer customer) {
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)" +
                            "VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());

            int ret = preparedStatement.executeUpdate();
            System.out.println("Execute update returns: " + ret);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return true;
    }

    /**
     * Contains Sql-query to update an existing customer in the DB.
     *
     * @param customerId     The id of the customer we want to update.
     * @param updateCustomer A customer object containing the update data.
     * @throws SQLException
     */
    public void updateCustomer(int customerId, Customer updateCustomer) {
        Customer oldCustomer = getCustomerById(customerId);
        connectionHelper.openConnection();
        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "UPDATE Customer " +
                            "SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? " +
                            "WHERE CustomerId = ?");

            /**
             * Conditions for finding which fields to update. If a field from updateCustomer is empty, the old
             * information is kept.
             */
            if (updateCustomer.getFirstName() == null || updateCustomer.getFirstName().isEmpty()) {
                preparedStatement.setString(1, oldCustomer.getFirstName());
            } else {
                preparedStatement.setString(1, updateCustomer.getFirstName());
            }

            if (updateCustomer.getLastName() == null || updateCustomer.getLastName().isEmpty()) {
                preparedStatement.setString(2, oldCustomer.getLastName());
            } else {
                preparedStatement.setString(2, updateCustomer.getLastName());
            }

            if (updateCustomer.getCountry() == null || updateCustomer.getCountry().isEmpty()) {
                preparedStatement.setString(3, oldCustomer.getCountry());
            } else {
                preparedStatement.setString(3, updateCustomer.getCountry());
            }

            if (updateCustomer.getPostalCode() == null || updateCustomer.getPostalCode().isEmpty()) {
                preparedStatement.setString(4, oldCustomer.getPostalCode());
            } else {
                preparedStatement.setString(4, updateCustomer.getPostalCode());
            }

            if (updateCustomer.getPhoneNumber() == null || updateCustomer.getPhoneNumber().isEmpty()) {
                preparedStatement.setString(5, oldCustomer.getPhoneNumber());
            } else {
                preparedStatement.setString(5, updateCustomer.getPhoneNumber());
            }

            if (updateCustomer.getEmail() == null || updateCustomer.getEmail().isEmpty()) {
                preparedStatement.setString(6, oldCustomer.getEmail());
            } else {
                preparedStatement.setString(6, updateCustomer.getEmail());
            }
            preparedStatement.setInt(7, customerId);

            int ret = preparedStatement.executeUpdate();
            System.out.println("Execute update returns: " + ret);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
    }

    /**
     * Contains Sql-query to count how many customers per country there are.
     *
     * @return An arrayList of customerPerCountry objects.
     * @throws SQLException
     */
    public ArrayList<CustomersInCountry> getCustomersPerCountry() {

        ArrayList<CustomersInCountry> customersPerCountry = new ArrayList<>();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement("" +
                    "SELECT COUNT(CustomerId) AS Customers, Country " +
                    "FROM Customer " +
                    "GROUP BY Country " +
                    "ORDER BY COUNT(CustomerId) DESC");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customersPerCountry.add(new CustomersInCountry(
                        resultSet.getInt("Customers"),
                        resultSet.getString("Country")
                ));
            }


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return customersPerCountry;
    }

    /**
     * Contains Sql-query to count how much money each customer has spent
     *
     * @return An arraylist containing highestCustomerSpender objects, in descending order of how much each customer spent.
     * @throws SQLException
     */
    public ArrayList<LargestCustomerSpender> getHighestCustomerSpender() {

        ArrayList<LargestCustomerSpender> highestLargestCustomerSpenders = new ArrayList<>();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement("" +
                    "SELECT CustomerId, SUM(Total) AS Total " +
                    "FROM Invoice " +
                    "GROUP BY CustomerId " +
                    "ORDER BY Total DESC");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                highestLargestCustomerSpenders.add(new LargestCustomerSpender(
                        resultSet.getInt("CustomerId"),
                        resultSet.getDouble("Total")
                ));
            }


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }

        return highestLargestCustomerSpenders;
    }

    /**
     * Contains an Sql-query to find a specific customers favourite genre.
     *
     * @param customerId the id of the customer we want to look up.
     * @return A mostPopularCustomerGenre object
     * @throws SQLException
     */
    public ArrayList<MostPopularCustomerGenre> getMostPopularCustomerGenre(int customerId) {

        ArrayList<MostPopularCustomerGenre> mostPopularCustomerGenre = new ArrayList<>();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "WITH t1 AS ( " +
                            "SELECT ge.Name, COUNT(ge.GenreId) AS GenreCount " +
                            "FROM Customer cu " +
                            "INNER JOIN Invoice inv ON cu.CustomerId = inv.CustomerId " +
                            "INNER JOIN InvoiceLine il ON inv.InvoiceId = il.InvoiceId " +
                            "INNER JOIN Track tr ON tr.TrackId = il.TrackId " +
                            "INNER JOIN Genre ge ON ge.GenreId = tr.GenreId " +
                            "WHERE cu.CustomerId = ? " +
                            "GROUP BY ge.GenreId) " +

                            "SELECT t1.* " +
                            "FROM t1 " +
                            "WHERE GenreCount = (SELECT MAX(GenreCount) FROM t1)");

            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                mostPopularCustomerGenre.add(new MostPopularCustomerGenre(
                        resultSet.getString("Name"),
                        resultSet.getInt("GenreCount")
                ));
            }


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            connectionHelper.closeConnection();
        }
        return mostPopularCustomerGenre;

    }
}
