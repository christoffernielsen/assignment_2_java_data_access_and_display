package com.example.assignment_2_java_data_access_and_display.models;

public class Artist {

    private final int id;
    private final String artistName;

    /**
     * Constructor for creating a new Artist.
     *
     * @param artistId   The ID of the Artist (primary key in database).
     * @param artistName The name of the Artist.
     */
    public Artist(int artistId, String artistName) {
        this.id = artistId;
        this.artistName = artistName;
    }

    /**
     * Method for getting the ID of the Artist.
     *
     * @return Returns the ID of the Artist as an int.
     */
    public int getId() {
        return id;
    }

    /**
     * Method for getting the name of the Artist.
     *
     * @return Returns name of the Artist as a String.
     */
    public String getArtistName() {
        return artistName;
    }
}
