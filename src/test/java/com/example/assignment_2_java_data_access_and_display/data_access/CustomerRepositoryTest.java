package com.example.assignment_2_java_data_access_and_display.data_access;

import com.example.assignment_2_java_data_access_and_display.models.Customer;
import com.example.assignment_2_java_data_access_and_display.models.CustomersInCountry;
import com.example.assignment_2_java_data_access_and_display.models.LargestCustomerSpender;
import com.example.assignment_2_java_data_access_and_display.models.MostPopularCustomerGenre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CustomerRepositoryTest {

    private CustomerRepository customerRepository;

    @BeforeEach
    void setUp() {
        customerRepository = new CustomerRepository();
    }

    @Test
    void getAllCustomers_shouldNotThrowSqlException() {
        assertDoesNotThrow(() -> customerRepository.getAllCustomers());
    }

    @Test
    void getAllCustomers_checkCustomer1InArrayList_shouldGetCorrectCustomerInfo() {
        ArrayList<Customer> customers = customerRepository.getAllCustomers();
        Customer customer = customers.get(0);
        assertEquals(1, customer.getId());
        assertEquals("Luís", customer.getFirstName());
        assertEquals("Gonçalves", customer.getLastName());
        assertEquals("Brazil", customer.getCountry());
        assertEquals("12227-000", customer.getPostalCode());
        assertEquals("+55 (12) 3923-5555", customer.getPhoneNumber());
        assertEquals("luisg@embraer.com.br", customer.getEmail());
    }

    @Test
    void getCustomerById_customerId1_shouldGetCorrectCustomerInfo() {
        Customer customer = customerRepository.getCustomerById(1);
        assertEquals(1, customer.getId());
        assertEquals("Luís", customer.getFirstName());
        assertEquals("Gonçalves", customer.getLastName());
        assertEquals("Brazil", customer.getCountry());
        assertEquals("12227-000", customer.getPostalCode());
        assertEquals("+55 (12) 3923-5555", customer.getPhoneNumber());
        assertEquals("luisg@embraer.com.br", customer.getEmail());
    }
    @Test
    void getCustomerByName_luisRojas_shouldGetCustomerInfoAboutLuisRojas() {
        Customer customer = customerRepository.getCustomerByName("Luis", "Rojas");
        assertEquals(57, customer.getId());
        assertEquals("Luis", customer.getFirstName());
        assertEquals("Rojas", customer.getLastName());
        assertEquals("Chile", customer.getCountry());
        assertNull(customer.getPostalCode());
        assertEquals("+56 (0)2 635 4444", customer.getPhoneNumber());
        assertEquals("luisrojas@yahoo.cl", customer.getEmail());
    }
    @Test
    void getOffsetCustomers_validOffsetValidLimit_shouldNotThrowSQLException (){
        assertDoesNotThrow(() -> customerRepository.getOffsetCustomers(10, 10));
    }

    @Test
    void addNewCustomer_newCustomerPontusRohden_shouldGetCorrectCustomerInfoAboutNewlyAddedCustomer(){
        Customer customer = new Customer(-1,"Pontus", "Rohden", "Sweden", "311 73", "0346-12345", "Pontus@experis");
        customerRepository.addNewCustomer(customer);

        Customer customerTest = customerRepository.getCustomerByName("Pontus", "Rohden");
        assertEquals("Pontus", customerTest.getFirstName());
        assertEquals("Rohden", customerTest.getLastName());
        assertEquals("Sweden", customerTest.getCountry());
        assertEquals("311 73", customerTest.getPostalCode());
        assertEquals("0346-12345", customerTest.getPhoneNumber());
        assertEquals("Pontus@experis", customerTest.getEmail());
    }
    @Test
    void updateCustomer_customerId17_shouldUpdateCustomer() {
        customerRepository.updateCustomer(17, new Customer(-1, "", "","Sweden" ,"311 73" ,"","" ));
        Customer customer = customerRepository.getCustomerById(17);
        assertEquals("Jack", customer.getFirstName());
        assertEquals("Smith", customer.getLastName());
        assertEquals("Sweden", customer.getCountry());
        assertEquals("311 73", customer.getPostalCode());
        assertEquals("+1 (425) 882-8080", customer.getPhoneNumber());
        assertEquals("jacksmith@microsoft.com", customer.getEmail());
    }

    @Test
    void getCustomersPerCountry_shouldNotThrowSQLException() {
        assertDoesNotThrow(() -> customerRepository.getCustomersPerCountry());
    }

    @Test
    void getCustomersPerCountry_checkCustomersPerCountry1InArrayList_shouldGetCorrectCustomersPerCountry() {
        ArrayList<CustomersInCountry> customersInCountry = customerRepository.getCustomersPerCountry();
        CustomersInCountry customerInCountry = customersInCountry.get(0);
        assertEquals(12, customerInCountry.getAmountOfCustomers());
        assertEquals("USA", customerInCountry.getCountry());
    }

    @Test
    void getHighestCustomerSpender_shouldNotThrowSQLException(){
        assertDoesNotThrow(() -> customerRepository.getHighestCustomerSpender());
    }

    @Test
    void getHighestCustomerSpender_checkHighestCustomerSpender1InArrayList_shouldGetCorrectHighestCustomerSpenderInformation() {
        ArrayList<LargestCustomerSpender> largestCustomerSpenders = customerRepository.getHighestCustomerSpender();
        LargestCustomerSpender largestCustomerSpender = largestCustomerSpenders.get(0);
        assertEquals(6, largestCustomerSpender.getCustomerId());
        assertEquals(49.620000000000005, largestCustomerSpender.getTotalCostInvoices());
    }

    @Test
    void getMostPopularCustomerGenre_withCustomerId6_shouldReturnRock() {
        ArrayList<MostPopularCustomerGenre> mostPopularCustomerGenres = customerRepository.getMostPopularCustomerGenre(6);
        assertEquals("Rock", mostPopularCustomerGenres.get(0).getGenre());
        assertEquals(10, mostPopularCustomerGenres.get(0).getCount());
    }
}