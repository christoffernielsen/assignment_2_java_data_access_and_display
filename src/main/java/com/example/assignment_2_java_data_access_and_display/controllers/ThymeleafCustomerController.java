package com.example.assignment_2_java_data_access_and_display.controllers;

import com.example.assignment_2_java_data_access_and_display.data_access.*;
import com.example.assignment_2_java_data_access_and_display.models.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;

@Controller
public class ThymeleafCustomerController {

    private final CustomerRepository customerRepository = new CustomerRepository();
    private final ArtistRepository artistRepository = new ArtistRepository();
    private final TrackRepository trackRepository = new TrackRepository();
    private final GenreRepository genreRepository = new GenreRepository();
    private final TrackInfoRepository trackInfoRepository = new TrackInfoRepository();

    /**
     * Used to fetch randomized objects on the homepage.
     *
     * @param model adds arraylists containing random artists, tracks and genres.
     * @return random artists, tracks and genres on the homepage.
     * @throws SQLException
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        ArrayList<Artist> artists = artistRepository.get5RandomArtists();
        model.addAttribute("artists", artists);

        ArrayList<Track> tracks = trackRepository.get5RandomTracks();
        model.addAttribute("tracks", tracks);

        ArrayList<Genre> genres = genreRepository.get5RandomGenres();
        model.addAttribute("genres", genres);

        return "index";
    }

    /**
     * Used to fetch all the customers in the DB.
     *
     * @param model Adds an arraylist containing all the customers.
     * @return The customers page populated whit all the customers in the DB
     * @throws SQLException
     */
    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String getAllCustomers(Model model) {
        ArrayList<Customer> customers = customerRepository.getAllCustomers();
        model.addAttribute("customers", customers);
        return "view-customers";
    }

    /**
     * Fetches a specific customer from the DB.
     *
     * @param model      adds and arraylist containing the customer.
     * @param customerId the id of the customer we're looking for.
     * @return a view of the customer, if found.
     * @throws SQLException
     */
    @RequestMapping(value = "/customer/{customerId}", method = RequestMethod.GET)
    public String getCustomerById(Model model, @PathVariable int customerId) {
        Customer customer = customerRepository.getCustomerById(customerId);
        model.addAttribute("customer", customer);
        return "view-customer";
    }

    /**
     * Used to open the addCustomer view.
     *
     * @param model adds an empty customer object.
     * @return A view to add a new customer.
     */
    @RequestMapping(value = "/customer/add", method = RequestMethod.GET)
    public String addCustomer(Model model) {
        Customer customer = new Customer();
        model.addAttribute("customer", customer);
        return "add-customer";
    }

    /**
     * Used to post a new customer to the DB.
     *
     * @param customer the customer we want to push to the DB.
     * @param model    used to add success-boolean and customer.
     * @return returns to the same page.
     * @throws SQLException
     */
    @RequestMapping(value = "/customer/add", method = RequestMethod.POST)
    public String addCustomer(@ModelAttribute Customer customer, Model model) {
        boolean success = customerRepository.addNewCustomer(customer);
        model.addAttribute("success", success);
        if (success) {
            model.addAttribute("customer", customer);
        }
        return "add-customer";
    }

    /**
     * Used to open the Search Track view.
     *
     * @return opens the Search track view.
     */
    @RequestMapping(value = "/track/search", method = RequestMethod.GET)
    public String searchForTrackByName() {
        return "view-tracks";
    }

    /**
     * Used to Search the DB for tracks by track name.
     *
     * @param searchString the search string added by the user.
     * @param model        adds an arrayslist of tracks with track names corresponding to the search string.
     * @return a view of the tracks found.
     * @throws SQLException
     */
    @RequestMapping(value = "/track/search", method = RequestMethod.POST)
    public String searchForTrackByName(@RequestParam("searchString") String searchString, Model model) {
        ArrayList<TrackInfo> trackInfos = trackInfoRepository.getTrackInfoBySearchingTrackName(searchString);
        model.addAttribute("success", true);
        model.addAttribute("searchString", searchString);
        model.addAttribute("trackInfo", trackInfos);
        return "view-tracks";
    }
}
