package com.example.assignment_2_java_data_access_and_display.models;

public class CustomersInCountry {

    private final int amountOfCustomers;
    private final String country;

    /**
     * Constructor for creating a new CustomersInCountry.
     *
     * @param amountOfCustomers The amount of customers in the specified country.
     * @param country           The country of the Customers.
     */
    public CustomersInCountry(int amountOfCustomers, String country) {
        this.amountOfCustomers = amountOfCustomers;
        this.country = country;
    }

    /**
     * Method for getting the amount of customers in a country.
     *
     * @return Returns the amount of customers in the country.
     */
    public int getAmountOfCustomers() {
        return amountOfCustomers;
    }

    /**
     * Method for getting the country of the CustomersInCountry.
     *
     * @return Returns the country as a String.
     */
    public String getCountry() {
        return country;
    }
}
