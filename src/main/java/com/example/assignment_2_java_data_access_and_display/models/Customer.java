package com.example.assignment_2_java_data_access_and_display.models;

public class Customer {

    private int id;
    private String firstName;
    private String lastName;
    private String country;
    private String postalCode;
    private String phoneNumber;
    private String email;

    /**
     * Constructor for creating a new Customer.
     *
     * @param id          The ID of the Customer (primary key in database).
     * @param firstName   The first name of the Customer.
     * @param lastName    The last name of the Customer.
     * @param country     The Customer's country.
     * @param postalCode  The postal code of the Customer.
     * @param phoneNumber The phone number to the Customer.
     * @param email       The E-mail to the Customer.
     */
    public Customer(int id, String firstName, String lastName, String country, String postalCode, String phoneNumber, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    /**
     * Empty constructor for creating a new Customer.
     * Used in combination with setters in POST request for adding a new Customer to the database.
     */
    public Customer() {
    }

    /**
     * Method for getting the ID of the Customer.
     *
     * @return Returns the ID of the Customer as an int.
     */
    public int getId() {
        return id;
    }

    /**
     * Method for getting the first name of the Customer.
     *
     * @return Returns the Customer's first name as a String.
     */
    public String getFirstName() {
        return firstName;
    }


    /**
     * Method for getting the last name of the Customer.
     *
     * @return Returns the Customer's last name as a String.
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * Method for getting the country of the Customer.
     *
     * @return Returns the Customer's country as a String.
     */
    public String getCountry() {
        return country;
    }


    /**
     * Method for getting the postal code of the Customer.
     *
     * @return Returns the Customer's postal code as a String.
     */
    public String getPostalCode() {
        return postalCode;
    }


    /**
     * Method for getting the phone number of the Customer.
     *
     * @return Returns the Customer's phone number as a String.
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }


    /**
     * Method for getting the e-mail of the Customer.
     *
     * @return Returns the Customer's e-mail as a String.
     */
    public String getEmail() {
        return email;
    }


    /**
     * Method for getting the full name of the Customer. Does this by adding first- and lastname.
     *
     * @return Returns the Customer's full name as a String.
     */
    public String getFullName() {
        return firstName + " " + lastName;
    }


    /**
     * Method for setting a Customers ID (this is automatically done by the database when trying to add a new Customer).
     *
     * @param id the ID of the Customer to be set.
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Method for setting the first name of the Customer.
     *
     * @param firstName the first name of the Customer to be set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    /**
     * Method for setting the last name of the Customer.
     *
     * @param lastName the last name of the Customer to be set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * Method for setting the country of the Customer.
     *
     * @param country the country of the Customer to be set.
     */
    public void setCountry(String country) {
        this.country = country;
    }


    /**
     * Method for setting the postal code of the Customer.
     *
     * @param postalCode the postal code of the Customer to be set.
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }


    /**
     * Method for setting the phone number of the Customer.
     *
     * @param phoneNumber the phone number of the Customer to be set.
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    /**
     * Method for setting the e-mail of the Customer.
     *
     * @param email the e-mail of the Customer to be set.
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
